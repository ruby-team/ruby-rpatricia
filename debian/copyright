Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rpatricia
Source: http://www.goto.info.waseda.ac.jp/~tatsuya/rpatricia/

Files: *
Copyright: 2007-2014 Tatsuya Mori <mori.tatsuya@gmail.com>
           2010-2014 Eric Wong <normalperson@yhbt.net>
License: LGPL-2.1+
 rpatricia is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at your
 option) any later version.
 .
 rpatricia is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with rpatricia.  If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems the full text of the GNU Lesser General Public License is
 available at /usr/share/common-licenses/LGPL-2.1 .

Files: ext/rpatricia/patricia.*
Copyright: Copyright (c) 1999-2013 The Regents of the University of Michigan
           ("The Regents") and Merit Network, Inc.
License: BSD-2-Clause

Files: debian/*
Copyright: 2014 Apollon Oikonomopoulos <apoikos@debian.org>
License: BSD-2-Clause

License: BSD-2-Clause
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
